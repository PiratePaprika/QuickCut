function getFileFormatFor(fileType)
{
    switch(fileType)
    {
        case "url":
            return "[InternetShortcut]\nURL=%url%";
        case "desktop":
            return "[Desktop Entry]\nVersion=1.0\nType=Link\nName=%title%\nURL=%url%";
        case "webloc":
            return "<plist version=\"1.0\"><dict>\n<key>URL</key><string>%url%</string>\n</dict></plist>";
        case "html":
            return "<html><head><meta http-equiv=\"refresh\" content=\"0; url=%url%\" \/></head></html>";
    }
}

function cleanTitle(title)
{
    return title
        .substring(0,200)
        .replace(/[*/\\•:#><;,"?|]+/g, " ")
        .replace(/\s\s+/g, ' ')
        .trim();
}

function saveLink(tab)
{
    browser.storage.local.get().then(
    (settings) =>
    {
        let title = cleanTitle(tab.title);
        let url = tab.url;
        if (settings.fileType == "webloc")
        {
            url = url.replaceAll("&amp;", "&")
            url = url.replaceAll("&", "&amp;");
        }

        let contents = getFileFormatFor(settings.fileType)
            .replaceAll("%url%", url)
            .replaceAll("%title%", tab.title);

        let filename = "";

        if (settings.savePath)
        {
            filename += settings.savePath + "/";
        }

        filename += `${title}.${settings.fileType}`;

        let file      = new File([contents],filename);
        let objectUrl = URL.createObjectURL(file);

        let downloadSettings = {
            url       : objectUrl,
            filename  : filename,
            incognito : tab.incognito
        }

        if (settings.saveAs !== null)
        {
            downloadSettings.saveAs = settings.saveAs;
        }

        browser.downloads.download(downloadSettings).then(null, (error) => console.log(error));
    })
}

function getOperatingSystemFileType()
{
    if (navigator.platform.startsWith("Linux"))
    {
        return "desktop";
    }
    else if (navigator.platform.startsWith("Mac"))
    {
        return "webloc";
    }
    else
    {
        return "url";
    }
}

function getDefaults()
{
    return {
        fileType    : getOperatingSystemFileType(),
        contextMenu : false,
        savePath    : "",
        saveAs      : null,
        pageAction  : false
    };
}

function setDefaults()
{
    browser.storage.local.clear();
    browser.storage.local.set( getDefaults() ).then(null);
}

function setDefaultsIfEmptyOrInvalid()
{
    return new Promise(
    (resolve) =>
    {
        browser.storage.local.get().then(
        (settings) =>
        {
            let length = Object.keys(settings).length
            if ( length === 0 || length != Object.keys(getDefaults()).length)
            {
                setDefaults();
            }

            resolve();
        })
      });
}

function createContextMenu()
{
    browser.contextMenus.create(
    {
        id: "save-link",
        title: "Save Link",
        contexts: ["all"]
    });
}

function removeContextMenu()
{
    browser.contextMenus.remove("save-link");
}

function setup()
{
    browser.storage.local.get().then(
    (settings) =>
    {
        setupContextMenu(settings.contextMenu);
    })
}

function setupContextMenu(on)
{
    if (on)
    {
        createContextMenu();
    }
    else
    {
        removeContextMenu();
    }
}

function settingsChanged(name, value)
{
    switch(name)
    {
        case "contextMenu":
            setupContextMenu(value);
            break;
    }
}

browser.storage.onChanged.addListener(
(changes) =>
{
    for (let name of Object.keys(changes))
    {
        if (changes[name].oldValue != changes[name].newValue)
        {
            settingsChanged(name, changes[name].newValue);
        }
    }
})

browser.contextMenus.onClicked.addListener((info, tab) => 
{
    if (info.menuItemId === "save-link")
    {
        saveLink(tab);
    }
});

browser.browserAction.onClicked.addListener((tab) => saveLink(tab));


function main()
{
    setDefaultsIfEmptyOrInvalid().then(setup)
}

main();
